<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<head>
<style>
span.error {
	color: red;
}

div.error {
	color: red;
}

div.errors {
background-color: #ffcccc;
border: 2px solid blue;
}
label.error {
color: red;
}
input.error {
background-color: #ffcccc;
}
</style>

</head>

<body>
<h1><s:message code="title.message"/></h1>
	<sf:form method="POST" modelAttribute="form">
	
	<sf:errors path="*" element="div" cssClass="errors" />

		<sf:label path="firstName" cssErrorClass="error">First Name</sf:label>:<sf:input path="firstName" cssErrorClass="error" />
		<br />
Last Name: <sf:input path="lastName"/>
		<sf:errors path="lastName" cssClass="error" />
		<br />
Email: <sf:input path="email" />
		<br />
Username: <sf:input path="username" />
		<br />
Password: <sf:password path="password" />
		<br />
		<input type="submit" value="Register" />
	</sf:form>
	
	<s:escapeBody htmlEscape="true">
<h1>Hello</h1>
</s:escapeBody>
<br/>
<s:escapeBody javaScriptEscape="true">
<h1>Hello</h1>
</s:escapeBody>
<br/>
<s:url value="/register/{username}" var="spitterUrl">
<s:param name="username" value="jbauer" />
</s:url>

<a href=${spitterUrl}>username</a>

<br/>
<s:url value="/register" var="spittlesUrl">
<s:param name="max" value="60" />
<s:param name="count" value="20" />
</s:url>

<a href=${spittlesUrl}>params url</a>
</body>
</html>