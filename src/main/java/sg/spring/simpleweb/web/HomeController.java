package sg.spring.simpleweb.web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import sg.spring.simpleweb.web.model.RegisterForm;

@Controller
@RequestMapping(value={"/"})
public class HomeController {

	@RequestMapping(value={"home",""},method=RequestMethod.GET)
	public String home() {
		return "home";
	}
	
	
	@RequestMapping(value={"register"},method={RequestMethod.GET})
	public String registerGet() {
		return "register";
	}
	
	@RequestMapping(value={"register"},method={RequestMethod.POST})
	public String registerPost(RegisterForm form, Model model) {
		model.addAttribute("message", form.toString());
		return "showMessage";
	}
	
	@RequestMapping(value="/list")
	public String listOfStr(Model model) {
		List<String> list = new ArrayList<>();
		list.add("one");
		list.add("two");
		list.add("three");
		model.addAttribute("alist", list);
		return "list";
	}
	
	@RequestMapping(value="/show")
	public List<String> showParams(@RequestParam(value="num", defaultValue="false") boolean showNum) {
		List<String> list = new ArrayList<>();
		list.add("one");
		list.add("two");
		list.add("three");
		if (showNum) return list;
		list.add("four");
		return list;
	}
	
	@RequestMapping(value="/show/{message}")
	public String showMessage(@PathVariable("message") String msg, Model model) {
		model.addAttribute("message", msg);
		return "showMessage";
	}
	
	@RequestMapping(value="/register/alt", method=RequestMethod.GET)
	public String registerAltGet(@ModelAttribute("form") RegisterForm form) {
		return "registeralt";
	}
	
	@RequestMapping(value="/register/alt", method=RequestMethod.POST)
	public String registerAlt(
			//@ModelAttribute("form") //this is adding org.springframework.validation.BindingResult.form attribute to model magically
			@Valid RegisterForm form1, BindingResult errors, Model model) {
		
		if (errors.hasErrors()) {
			model.addAttribute("form", form1);
			model.addAttribute("org.springframework.validation.BindingResult.form", errors);
			System.out.println("Model: "+model);
			return "registeralt";
		} else {
			//model.addAttribute("form", form1);
			//org.springframework.validation.BindingResult.form
			model.addAttribute("message", "Registered:"+form1);
			return "showMessage";
		}
		
	}
	
	@RequestMapping(value="/upload", method=RequestMethod.GET)
	public String uploadFileGet() {
		return "upload";
	}
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	public String uploadFile(@RequestParam("file") MultipartFile file, Model model) throws IllegalStateException, IOException {
		file.transferTo(new File("C:\\temp\\uploads\\hello.txt"));
		model.addAttribute("message", file.getOriginalFilename());
		return "showMessage";
	}
}
