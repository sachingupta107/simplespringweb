package sg.spring.simpleweb.web;

import java.util.Arrays;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerController {
	@ExceptionHandler(Exception.class)
	public ModelAndView consumeForm(
			Exception e
			) {
		System.out.println("Controller advice called");
		ModelAndView model = new ModelAndView("showMessage");
		model.addObject("message", Arrays.toString(e.getStackTrace()));
		System.out.println("reqString: "+e.toString());
		return model;
		}
		
	
	
}