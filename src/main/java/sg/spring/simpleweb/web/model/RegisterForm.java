package sg.spring.simpleweb.web.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterForm {
	//customize location of message resolution for message properties: 
	//http://stackoverflow.com/questions/4186556/hibernate-validator-custom-resourcebundlelocator-and-spring
	@NotNull
	@Size(min=5, max=100, message="{firstname.size}")
	String firstName;
	@NotNull
	@Size(min=5, max=100)
	String lastName;
	String username;
	String password;
	String email;
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String userName) {
		this.username = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "RegisterForm [firstName=" + firstName + ", lastName="
				+ lastName + ", userName=" + username + ", password="
				+ password + "]";
	}

}
