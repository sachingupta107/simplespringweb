package sg.spring.simpleweb.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("one")
public class OneController {

	@RequestMapping("/one")
	public String oneOne(Model model) {
		model.addAttribute("message","oneOne");
		return "showMessage";
	}
	
//	@RequestMapping("one")
//	public String one(Model model) {
//		model.addAttribute("message","oneOne");
//		return "showMessage";
//	}
	
	@RequestMapping("")
	public String oneSlash(Model model) {
		model.addAttribute("message","/");
		return "showMessage";
	}
	
	
}
