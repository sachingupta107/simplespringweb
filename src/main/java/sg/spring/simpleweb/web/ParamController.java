package sg.spring.simpleweb.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import sg.spring.simpleweb.web.exceptions.ItemNotFoundException;
import sg.spring.simpleweb.web.model.RegisterForm;

@Controller
@RequestMapping("/param")
public class ParamController {
	@RequestMapping(value="/form", method=RequestMethod.POST)
	public String consumeForm(
			@RequestBody String reqString
			,@ModelAttribute("form") RegisterForm reqString2
			, Model m
			) {
		m.addAttribute("message", reqString+"<br/>"+reqString2);
		if (reqString2.getFirstName() == null || reqString2.getFirstName().equals("")) {
			throw new ItemNotFoundException();
		}
		System.out.println("reqString: "+reqString);
		return "showMessage";
	}
	
	@RequestMapping(value="/form", method=RequestMethod.GET)
	public String consumeForm(@ModelAttribute("form") RegisterForm reqString) {
		return "registeralt";
	}
	
//	@ExceptionHandler(Exception.class)
//	public ModelAndView formException(Exception e) {
////		model.addAttribute("message", "item not found");
//		System.out.println("item not found:"+e);
//		ModelAndView model = new ModelAndView("showMessage");
//		model.addObject("message", "e.getMessage()");
//		return model;
//	}
}
