package sg.spring.simpleweb.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
@ImportResource("classpath:spring/application-config.xml")
public class RootConfig {

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource
				.setBasenames(new String[] { "classpath:messages/messages",
						"classpath:messages/messages1" });
		messageSource.setCacheSeconds(-1); // only -1 will work with classpath
		return messageSource;
	}

//	@Bean
//	public MessageSource messageSource() {
//		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//		messageSource.setBasename("messages");
//		return messageSource;
//	}
}

/*
 * 
 * @Configuration
 * 
 * @ComponentScan(basePackages={"spitter"}, excludeFilters={
 * 
 * @Filter(type=FilterType.ANNOTATION, value=EnableWebMvc.class) }) public class
 * RootConfig { }
 */
