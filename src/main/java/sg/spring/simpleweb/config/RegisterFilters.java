package sg.spring.simpleweb.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;

import sg.spring.simpleweb.filter.Filter1;

public class RegisterFilters implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		System.out.println("register filters here");
		javax.servlet.FilterRegistration.Dynamic filter =
				servletContext.addFilter("myFilter", Filter1.class);
				filter.addMappingForUrlPatterns(null, false, "/*");
		
	}

}
