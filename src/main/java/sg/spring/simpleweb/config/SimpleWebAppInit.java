package sg.spring.simpleweb.config;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

@Configuration
public class SimpleWebAppInit extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { RootConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebConfig.class };
	}

	// @Override
	// protected Filter[] getServletFilters() {
	// return new Filter[] { new DispatcherServletFilter1() };
	// }
	@Override
	protected void customizeRegistration(Dynamic registration) {
		MultipartConfigElement multipartConfig = new MultipartConfigElement("c:/temp/uploads");
//		public MultipartConfigElement(java.lang.String location,
//                long maxFileSize,
//                long maxRequestSize,
//                int fileSizeThreshold)
		multipartConfig.getMaxFileSize();
		registration.setMultipartConfig(multipartConfig);
	}
}
