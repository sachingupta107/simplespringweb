package sg.spring.simpleweb.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;

public class RegisterServlets implements WebApplicationInitializer {
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		System.out.println("register servlets here");
		// Dynamic myServlet = servletContext.addServlet("myServlet",
		// MyServlet.class);
		// myServlet.addMapping("/custom/**");
	}
}
